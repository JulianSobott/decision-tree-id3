import math


def entropy(data: list[int]):
    """

    Entropy(S) = - sum p_i * log2(p_i)

    :param S: set of examples
    :return:
    """
    return -sum([i / sum(data) * math.log2(i / sum(data)) for i in data if i != 0])


def information_gain(data: list[list[int]]):
    """
    Information Gain(S, A) = Entropy(S) - sum |Sv|/|S| * Entropy(Sv)
    where Sᵥ is the set of rows in S for which the feature column A has value v, |Sᵥ| is the number of rows in Sᵥ and likewise |S| is the number of rows in S.
    :return:
    """
    num_unique_values = len(data[0])
    entropy_attribute = entropy([sum([values[i] for values in data]) for i in range(num_unique_values)])
    return entropy_attribute - sum(
        [sum(values) / sum([sum(values) for values in data]) * entropy(values) for values in data])


if __name__ == "__main__":
    # example data
    # sonnig(2*Y, 3*N) = 0.971
    # bewoelkt(4*Y, 0*N) = 0
    # regnerisch(3*Y, 2*N) = 0.971
    # wetter(9*Y, 5*N) = 0.94
    print(entropy([4, 4, 4, 4]))
    print(information_gain([[2, 3], [4, 0], [3, 2]]))
