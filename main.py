import pandas as pd

from calc_builder import CalcBuilder
from model import Attribute, Value, Layer, Data, TreeNode
from entropy import entropy, information_gain
from renderer import render_task, generate_tikz_definition
import os
import shutil


images_folder = "latex/latex/images/"


def read_data(data_path):
    return pd.read_csv(data_path)


def solve_task(data_path: str = "data.csv"):
    data: pd.DataFrame = read_data(data_path)
    # data format
    # Nr,frühere_kreditwürdigkeit,verschuldung,sicherheit,einkommen,zielattribut
    # Nr can be ignored
    # zielattribut is the target
    # the other all attributes

    attributes = data.columns[1:-1]
    builder = CalcBuilder()

    shutil.rmtree(images_folder, ignore_errors=True)
    os.makedirs(images_folder, exist_ok=True)

    tree = build_tree(data, attributes, builder)
    print(builder.layers)
    render_task(Data(builder.layers))
    return


def build_tree(data: pd.DataFrame, attributes: list[str], builder: CalcBuilder):
    # if all target values are the same, return the target value
    if len(data["zielattribut"].unique()) == 1:
        target_value = data["zielattribut"].unique()[0]
        builder.finalize_tree(target_value)
        return target_value
    # if there are no attributes left, return the most common target value
    if len(attributes) == 0:
        target_value = data["zielattribut"].value_counts().idxmax()
        #builder.finalize_tree(target_value)
        return target_value

    igs = []
    for attribute in attributes:
        ig = calc_for_one_attribute(data, attribute, builder)
        igs.append(ig)

    # attribute with the highest information gain
    selected_attribute = attributes[igs.index(max(igs))]
    builder.select_attribute(selected_attribute)

    tree = {selected_attribute: {}}

    # recursively build the decision tree for each attribute value
    for value in data[selected_attribute].unique():
        builder.select_value(value)
        rows = data.loc[data[selected_attribute] == value]
        tree[selected_attribute][value] = build_tree(rows, [a for a in attributes if a != selected_attribute], builder)
        builder.de_select_value()

    builder.finalize_build_tree()

    return tree


def calc_for_one_attribute(data, attribute: str, builder: CalcBuilder):
    unique_target_values = data["zielattribut"].unique()
    unique_values = data[attribute].unique()
    values = []
    for value in unique_values:
        # get all rows with this value
        rows = data.loc[data[attribute] == value]
        # count the target values
        v = []
        v_with_labels: list[tuple[str, int]] = []
        for target_value in unique_target_values:
            # get all rows with this target value
            rows_target = rows.loc[rows["zielattribut"] == target_value]
            # count the rows
            v.append(len(rows_target))
            v_with_labels.append((target_value, len(rows_target)))
        builder.entropy_for_value(value, v_with_labels)
        values.append(v)

    information_gain_attribute = information_gain(values)
    num_unique_values = len(unique_target_values)
    entropy_attribute = entropy([sum([v[i] for v in values]) for i in range(num_unique_values)])
    builder.finalize_attribute(attribute, information_gain_attribute, entropy_attribute)
    return information_gain_attribute


if __name__ == '__main__':
    solve_task("data.csv")
