from dataclasses import dataclass


@dataclass
class TreeNode:
    text: str
    is_leaf: bool
    children: list[tuple[str, 'TreeNode']] = None

    def copy(self):
        return TreeNode(self.text, self.is_leaf, self.children.copy() if self.children is not None else None)


@dataclass
class Data:
    layers: list["Layer"]


@dataclass
class Layer:
    pre_conditions: list["PreCondition"]
    attributes: list["Attribute"]
    current_tree: str
    only_target: str = ""
    best_attribute: "Attribute" = None

    def __post_init__(self):
        if self.best_attribute is None and len(self.attributes) > 0:
            self.best_attribute = self.attributes[0]
            for attribute in self.attributes:
                if attribute.information_gain > self.best_attribute.information_gain:
                    self.best_attribute = attribute


@dataclass
class PreCondition:
    attribute: str
    value: str


@dataclass
class Attribute:
    name: str
    values: list["Value"]
    entropy: float
    information_gain: float
    total: int


@dataclass
class Value:
    name: str
    values: list[tuple[str, int]]   # (zielattribut, count)
    entropy: float
    total: int = None

    def __post_init__(self):
        self.total = sum([count for _, count in self.values])
