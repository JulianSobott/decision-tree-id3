# Decision Tree with ID3

Generates a step by step calculation of the ID3 algorithm for a given dataset.
Renders the calculations + decision trees at each point in a PDF file.

## Requirements

- Python 3.11 (older versions may work, but are untested)
- Poetry
- Docker
- Make

## Installation

```bash
make deps
```

## Usage

```bash
make
```

You can replace the data in [data.csv](data.csv) with your own data.\
For now the column name `zielattribut` is hardcoded and can't be changed.\

*For usage or installation without make execute the commands manually from [Makefile](Makefile).*

## Development

The template for the generated latex file is in [templates/main.tex.j2](templates/main.tex.j2).\
[main.py](main.py) is the entrypoint for the program and contains the id3 algorithm.\
[entropy.py](entropy.py) contains the entropy and information gain calculations.\
[model.py](model.py) contains the models (dataclasses) for the generation of the jinja2 template.\
[renderer.py](renderer.py) contains the functions for rendering the jinja2 template and decision tree.\
[calc_builder.py](calc_builder.py) contains the functions for building the calculation steps. This one is used to keep track of all calculations, so that at the end every step can be rendered.

Data is read from [data.csv](data.csv).\
The generated pdf is saved to `latex/pdf/main.pdf`.

## Examples

Example data and the respective generated pdf can be found in [examples](examples).
