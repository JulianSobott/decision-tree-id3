from model import Attribute, Value, Layer, Data, TreeNode, PreCondition
from entropy import entropy, information_gain
from renderer import render_tree


class CalcBuilder:

    def __init__(self):
        self.current_attribute = None
        self.current_values = []
        self.current_attributes = []
        self.selected_pre_conditions = []
        self.layers = []
        self.current_node = None
        self.trees = []
        self.current_value = None
        self.root_node = None

    def entropy_for_value(self, value_name: str, values: list[tuple[str, int]]):
        entropy_value = entropy([count for _, count in values])
        v = Value(value_name, values, entropy_value)
        self.current_values.append(v)

    def finalize_attribute(self, attribute_name: str, information_gain_attribute: float, entropy_attribute: float):
        a = Attribute(
            attribute_name,
            self.current_values,
            entropy=entropy_attribute,
            information_gain=information_gain_attribute,
            total=sum([sum([count for _, count in values.values]) for values in self.current_values])
        )
        self.current_attributes.append(a)
        self.current_values = []

    def select_value(self, value_name: str):
        self.current_value = value_name
        self.selected_pre_conditions.append(PreCondition(self.current_attribute, value_name))

    def de_select_value(self):
        self.selected_pre_conditions.pop()

    def select_attribute(self, selected_attribute):
        self.current_attribute = selected_attribute
        node = TreeNode(selected_attribute, False, [])
        if self.current_node is None:
            self.current_node = node
        else:
            self.current_node.children.append((self.current_value, node))
        if self.root_node is None:
            self.root_node = node
        tree_str = render_tree(self.root_node)
        self.layers.append(Layer(self.selected_pre_conditions.copy(), self.current_attributes.copy(), tree_str))
        self.current_attributes = []
        self.trees.append(self.current_node)
        self.current_node = node

    def finalize_tree(self, target_value):
        node = TreeNode(target_value, True)
        self.current_node.children.append((self.current_value, node))
        tree_str = render_tree(self.root_node)
        self.layers.append(Layer(self.selected_pre_conditions.copy(), self.current_attributes.copy(), tree_str, target_value))
        self.current_attributes = []

    def finalize_build_tree(self):
        self.current_node = self.trees.pop()
