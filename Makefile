.PHONY: all
all: latex pdf


.PHONY: latex
latex:
	poetry run python main.py

.PHONY: pdf
pdf:
	docker run --rm -v "`pwd`/latex/latex":/src -v "`pwd`/latex/pdf":/pdf latex-build
	sudo chmod 777 latex/pdf/*

.PHONY: clean
clean:
	rm -rf latex/latex
	rm -rf latex/pdf/*


.PHONY: deps
deps: python-deps image

.PHONY: python-deps
python-deps:
	poetry install

.PHONY: image
image:
	docker build -t latex-build latex/
