import uuid

from jinja2 import Environment, FileSystemLoader
import networkx as nx
import matplotlib.pyplot as plt
import pygraphviz as pgv

from model import Attribute, Value, Layer, Data, TreeNode

templates = Environment(loader=FileSystemLoader("templates"))

default_destination = "latex/latex/main.tex"


def render_task(data, dest=default_destination):
    template = templates.get_template("main.tex.j2")
    res = template.render(data=data)
    with open(dest, "w") as f:
        f.write(res)


def render_tree(tree: TreeNode, dest=default_destination):
    name = uuid.uuid4().hex
    graph = pgv.AGraph(directed=True, strict=True)

    create_networkx_tree(graph, None, tree)

    graph.layout(prog='dot')
    graph.draw(f"latex/latex/images/{name}.png")
    # pos = nx.nx_agraph.graphviz_layout(graph, prog='dot')
    # labels = nx.get_node_attributes(graph, 'label')
    # leaf_nodes = [node for node in graph.nodes if graph.nodes[node]["is_leaf"]]
    # node_colors = ['lightgreen' if node in leaf_nodes else 'lightblue' for node in graph.nodes]
    # node_sizes = [len(label) * 300 for label in labels.values()]
    # nx.draw(graph, pos, with_labels=True, labels=labels, arrows=True, node_color=node_colors, edge_color='gray', font_weight='bold', node_shape='s', node_size=node_sizes)
    #
    # edge_labels = nx.get_edge_attributes(graph, 'label')
    # nx.draw_networkx_edge_labels(graph, pos, edge_labels=edge_labels)
    #


    # plt.savefig(f"latex/latex/images/{name}.png")
    # plt.clf()
    return name


def create_networkx_tree(graph, parent_key, node, edge_label=None):
    key = uuid.uuid4().hex
    graph.add_node(key, label=node.text, fillcolor="lightblue" if not node.is_leaf else "lightgreen", style="filled")

    if parent_key is not None:
        graph.add_edge(parent_key, key, label=edge_label)

    if node.is_leaf:
        return

    for child_text, child_node in node.children:
        create_networkx_tree(graph, key, child_node, child_text)


def generate_tikz_definition(node: TreeNode, indent: int = 0) -> str:
    tikz_code = ''

    if node.children:

        for condition, child_node in node.children:
            node_class = "leaf" if child_node.is_leaf else "default"
            tikz_code += f"\n{' ' * indent}child {{"
            tikz_code += f"\n{' ' * (indent + 2)}node [{node_class}] {{ {child_node.text} }}"
            # tikz_code += f"\n{' ' * (indent + 2)}node {{ {child_node.text} }}"
            child_tikz_code = generate_tikz_definition(child_node, indent + 4)
            tikz_code += f"{child_tikz_code}"
            tikz_code += f"\n{' ' * (indent + 2)}edge from parent node[above] {{ {condition} }}"

            tikz_code += f"\n{' ' * indent}}}"

    return tikz_code


if __name__ == "__main__":
    tree = TreeNode(
        text="Root",
        is_leaf=False,
        children=[
            ("Condition 1", TreeNode(text="Child 1", is_leaf=True)),
            ("Condition 2", TreeNode(text="Child 2", is_leaf=True)),
            ("Condition 3", TreeNode(text="Child 3", is_leaf=False, children=[
                ("Subcondition 1", TreeNode(text="Subchild 1", is_leaf=True)),
                ("Subcondition 2", TreeNode(text="Subchild 2", is_leaf=True))
            ]))
        ]
    )
    rendered_tree = f"\\node{{ {tree.text} }}" + generate_tikz_definition(tree) + ";"
    render_task(
        Data(
            [
                Layer(
                    [
                        Attribute(
                            "Wetter",
                            [
                                Value("Sonnig", 2, 3, 0.971),
                                Value("Bewölkt", 4, 0, 0),
                                Value("Regnerisch", 3, 2, 0.971),
                            ],
                            0.94,
                            0.246,
                            14,
                        )
                    ],
                    rendered_tree
                )
            ]
        )
    )
